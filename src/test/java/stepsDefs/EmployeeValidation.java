package stepsDefs;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import utils.DBUtils;
import java.util.List;

import  static  org.testng.Assert.*;

public class EmployeeValidation {

    private static String q;
    private static List<List<Object>> actualFirstAndLastName;

    @Given("user executes {string} query")
    public void userExecutesQuery(String query) {
        DBUtils.createDBConnection();
        q = query;
    }

    @Then("validate list of employees")
    public void validate_list_of_employees(DataTable employeesFAndL) {
        DBUtils.getQueryResultList(q); //actual
        employeesFAndL.asLists();       // expected

        for (int i = 0; i < employeesFAndL.asLists().size(); i++) {
            assertEquals(
                    DBUtils.getQueryResultList(q).get(i).get(0),
                    employeesFAndL.asLists().get(i).get(0)
                    );
            assertEquals(
                    DBUtils.getQueryResultList(q).get(i).get(1),
                    employeesFAndL.asLists().get(i).get(1)
            );
        }
    }
}
