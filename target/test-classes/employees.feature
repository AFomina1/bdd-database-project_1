Feature: validate employees

  @dataBaseTest
  Scenario: employee validation to database
    Given user executes "select first_name, last_name from employees where manager_id=122" query
    Then validate list of employees
      | Jason    | Mallin     |
      | Michael  | Rogers     |
      | Ki       | Gee        |
      | Hazel    | Philtanker |
      | Kelly    | Chung      |
      | Jennifer | Dilly      |
      | Timothy  | Gates      |
      | Randall  | Perkins    |

